import {Animal,DomesticatedAnimal,Pet} from './beast.js';

let animal = new Animal("Rawwr");
let dAnimal = new DomesticatedAnimal("Jonh Smith","Myaw",24868);
let pet = new Pet("Oleg Ivanov","Murzik",63830,"Woof");
//Using DoSound that declared only in Animal class
console.log(animal.DoSound());
console.log(dAnimal.DoSound());
console.log(pet.DoSound());
//using AddAge that declared in Animal class
animal.AddAge();
dAnimal.AddAge();
pet.AddAge();
//info
console.log(dAnimal.info);
console.log(pet.info);
//Using get food
animal.GetFood();
dAnimal.GetFood();
pet.GetFood();
//Using vaccination from pet that decared in DomesticatedAnimal
pet.DoVaccination();
//Using pet's methods
console.log(pet.PlayWithOwner());
console.log(pet.WalkWithOwner());
pet.PutOnaLeash();
console.log(pet.WalkWithOwner());
pet.RemoveaLeash();
console.log(pet.PlayWithOwner());