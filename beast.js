//Animal class
export class Animal{
    _age;//int
    _sound;//string
    _isHungry;//bool
    
    constructor(sound){
        this._age = 0;
        this._sound = sound;
        this._isHungry = false;
        this.GetFood();
    }

    // return sound of this beast
    DoSound(){
        return this._sound;
    }
    //Increase age
    AddAge(){
        this._age++;
    }
    //Get age
    get age(){
        return this._age;
    }
    //Become hunfry for dynamics in GetFood method
    BecomeHungry(){
        this._isHungry = true;
    }
    //Set hunger to false and create timer for another eating
    GetFood(){
        this._isHungry = false;

        setTimeout(()=>{
            this.BecomeHungry();
        },30000);
    }
}
//Domesticated Animal class 
export class DomesticatedAnimal extends Animal{

    _owner;//string
    _isVaccined;//bool
    _id;//int

    constructor(owner,sound,id){
        super(sound);//animal constructor
        this._owner = owner;
        this._isVaccined = false;
        this._id = id;
    }
    //Return info about owner
    get owner(){
        return `Owner is ${this._owner}`;
    }
    //Return info about this animal
    get info(){
        return `ID: ${this._id}
Owner: ${this._owner}
Age: ${this._age}
Vaccined: ${this._isVaccined}`;
    }
    //Set hunger to false and create timer for another eating
    GetFood(){
        this.isHungry = false;
        console.log(`Animal ${this._id} is hungry`);
        setTimeout(()=>{
            this.BecomeHungry();
        },30000);
    }
    //Set vaccine to true
    DoVaccination(){
        this.isVaccined = true;
    }

}
//Cat class
export class Pet extends DomesticatedAnimal{

    _name;//string
    _isOnaLeash;//bool
    _happiness;//double

    
    constructor(owner,name,id,sound){
        super(owner,sound,id);
        this._name = name;
        this._happiness = 0;
        this._isOnaLeash = false;
    }
    
    //override super get info
    get info(){
        return `ID: ${this._id}
Owner: ${this._owner}
Name: ${this._name}
Age: ${this.age}
Vaccined: ${this._isVaccined}`;
    }
    //Play to increase happiness
    PlayWithOwner(){
        if(this._isOnaLeash)
            return `First you need to remove a leash`;
        if(this.isHungry)
            return `First you need to feed your pet`;
        if(this._happiness<=1)
            return `Pet is unhappy, first you need to walk with it`;
        this._happiness+=1.5;
        return `You are successfully play with pet`;
    }
    //Walk to increase happiness
    WalkWithOwner(){
        if(!this._isOnaLeash)
            return `First you need to put on a leash`;
        if(this.isHungry)
            return `First you need to feed your pet`;
        this._happiness+=1;
        return `You are successfully walk with pet`;
    }
    //Change a leash status
    PutOnaLeash(){
        this._isOnaLeash = true;
    }
    //Change a leash status
    RemoveaLeash(){
        this._isOnaLeash = false;
    }



}
